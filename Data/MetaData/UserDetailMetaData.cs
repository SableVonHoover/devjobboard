﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Data //.MetaData
{
    [MetadataType(typeof(UserDetailMetaData))]
    public partial class UserDetail { }

    public class UserDetailMetaData
    {

        public string UserID { get; set; }

        [Required(ErrorMessage = "* First Name is required")]
        [Display(Name = "First Name")]
        [StringLength(50, ErrorMessage = "* First Name cannot exceed 50 characters")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "* Last Name is required")]
        [Display(Name = "Last Name")]
        [StringLength(50, ErrorMessage = "* Last Name cannot exceed 50 characters")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "* A Resume is required")]
        [Display(Name = "Resume")]
        [StringLength(75, ErrorMessage = "* File name cannot exceed 75 characters")]
        public string ResumeFilename { get; set; }



    }

}
