﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Data //.MetaData
{
    [MetadataType(typeof(ApplicationMetaData))]
    public partial class Application { }

    public class ApplicationMetaData
    {

        public int ApplicationID { get; set; }


        public int OpenPositionID { get; set; }

        [Display(Name = "User's ID")]
        [Required(ErrorMessage = "* ID is required")]
        public string UserID { get; set; }

        [Required(ErrorMessage = "* Date is required")]
        [Display(Name = "Date Applied")]
        public System.DateTime ApplicationDate { get; set; }

        [Display(Name = "Company Notes")]
        [StringLength(2000, ErrorMessage = "* Notes cannot exceed 2000 characters")]
        [DisplayFormat(NullDisplayText = "N/A")]
        public string ManagerNotes { get; set; }

        [Display(Name = "Is Declined?")]
        public bool IsDeclined { get; set; }

        [Display(Name = "Resume")]
        [StringLength(75, ErrorMessage = "* File name cannot exceed 75 characters")]
        [DisplayFormat(NullDisplayText = "N/A")]
        public string ResumeFilename { get; set; }




    }
}
