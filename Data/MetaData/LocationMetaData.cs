﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Data //.MetaData
{
    [MetadataType(typeof(LocationMetaData))]
    public partial class Location { }

    public class LocationMetaData
    {

        public int LocationID { get; set; }

        [Required(ErrorMessage = "* Company Name is required")]
        [Display(Name = "Company Name")]
        [StringLength(50, ErrorMessage = "* Name cannot exceed 50 characters")]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "* City is required")]
        [Display(Name = "City")]
        [StringLength(50, ErrorMessage = "* City cannot exceed 50 characters")]
        public string City { get; set; }

        [Required(ErrorMessage = "* State is required")]
        [Display(Name = "State")]
        [StringLength(2, ErrorMessage = "* Use the 2 character abbreviation")]
        public string State { get; set; }

        [Required]
        public string CompanyID { get; set; }

        [DisplayFormat(NullDisplayText = "N/A")]
        public string CompanyPic { get; set; }



    }


}
