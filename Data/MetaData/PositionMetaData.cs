﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Data //.MetaData
{
    [MetadataType(typeof(PositionMetaData))]
    public partial class Position { }

    public class PositionMetaData
    {

        public int PositionID { get; set; }

        [Required(ErrorMessage = "* Position Title is required")]
        [Display(Name = "Position Title")]
        [StringLength(50, ErrorMessage = "* Title cannot exceed 50 characters")]
        public string Title { get; set; }

        [DisplayFormat(NullDisplayText = "N/A")]
        public string JobDescription { get; set; }


    }

}
