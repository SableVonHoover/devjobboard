﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.IO;

namespace Data.Services
{
    public class ImageServices
    {

        public static void ResizeImage(string savePath, string fileName, Image image, int maxImgSize)
        {
            int[] newImageSizes = GetNewSize(image.Width, image.Height, maxImgSize);

            Bitmap newImage = DoResizeImage(newImageSizes[0], newImageSizes[1], image);

            newImage.Save(savePath + fileName);

            newImage.Dispose();
            image.Dispose();
        }

        public static int[] GetNewSize(int imgWidth, int imgHeight, int maxImgSize)
        {
            float ratioX = (float)maxImgSize / (float)imgWidth;
            float ratioY = (float)maxImgSize / (float)imgHeight;
            float ratio = Math.Min(ratioX, ratioY);

            int[] newImgSizes = new int[2];
            newImgSizes[0] = (int)(imgWidth * ratio);
            newImgSizes[1] = (int)(imgHeight * ratio);

            return newImgSizes;

        }

        public static Bitmap DoResizeImage(int imgWidth, int imgHeight, Image image)
        {
            Bitmap newImage = new Bitmap(imgWidth, imgHeight, PixelFormat.Format24bppRgb);

            newImage.MakeTransparent();

            // Set image to screen resolution of 72 dpi (the resolution of monitors)
            newImage.SetResolution(72, 72);

            using (Graphics graphics = Graphics.FromImage(newImage))
            {
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.DrawImage(image, 0, 0, imgWidth, imgHeight);
            }

            return newImage;
        }

        public static void Delete(string path, string imgName)
        {
            FileInfo fullImg = new FileInfo(path + imgName);

            if (fullImg.Exists)
            {
                fullImg.Delete();
            }

        }

    }
}
