IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenPositions_Positions]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenPositions]'))
ALTER TABLE [dbo].[OpenPositions] DROP CONSTRAINT [FK_OpenPositions_Positions]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenPositions_Locations]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenPositions]'))
ALTER TABLE [dbo].[OpenPositions] DROP CONSTRAINT [FK_OpenPositions_Locations]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]'))
ALTER TABLE [dbo].[AspNetUserRoles] DROP CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]'))
ALTER TABLE [dbo].[AspNetUserRoles] DROP CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserLogins]'))
ALTER TABLE [dbo].[AspNetUserLogins] DROP CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserClaims]'))
ALTER TABLE [dbo].[AspNetUserClaims] DROP CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Applications_OpenPositions]') AND parent_object_id = OBJECT_ID(N'[dbo].[Applications]'))
ALTER TABLE [dbo].[Applications] DROP CONSTRAINT [FK_Applications_OpenPositions]
GO
/****** Object:  Table [dbo].[UserDetails]    Script Date: 10/23/2018 2:23:17 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserDetails]') AND type in (N'U'))
DROP TABLE [dbo].[UserDetails]
GO
/****** Object:  Table [dbo].[Positions]    Script Date: 10/23/2018 2:23:17 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Positions]') AND type in (N'U'))
DROP TABLE [dbo].[Positions]
GO
/****** Object:  Table [dbo].[OpenPositions]    Script Date: 10/23/2018 2:23:17 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OpenPositions]') AND type in (N'U'))
DROP TABLE [dbo].[OpenPositions]
GO
/****** Object:  Table [dbo].[Locations]    Script Date: 10/23/2018 2:23:17 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Locations]') AND type in (N'U'))
DROP TABLE [dbo].[Locations]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 10/23/2018 2:23:17 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUsers]') AND type in (N'U'))
DROP TABLE [dbo].[AspNetUsers]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 10/23/2018 2:23:17 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]') AND type in (N'U'))
DROP TABLE [dbo].[AspNetUserRoles]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 10/23/2018 2:23:17 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUserLogins]') AND type in (N'U'))
DROP TABLE [dbo].[AspNetUserLogins]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 10/23/2018 2:23:17 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUserClaims]') AND type in (N'U'))
DROP TABLE [dbo].[AspNetUserClaims]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 10/23/2018 2:23:17 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetRoles]') AND type in (N'U'))
DROP TABLE [dbo].[AspNetRoles]
GO
/****** Object:  Table [dbo].[Applications]    Script Date: 10/23/2018 2:23:17 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Applications]') AND type in (N'U'))
DROP TABLE [dbo].[Applications]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 10/23/2018 2:23:17 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[__MigrationHistory]') AND type in (N'U'))
DROP TABLE [dbo].[__MigrationHistory]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 10/23/2018 2:23:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[__MigrationHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Applications]    Script Date: 10/23/2018 2:23:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Applications]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Applications](
	[ApplicationID] [int] IDENTITY(1,1) NOT NULL,
	[OpenPositionID] [int] NOT NULL,
	[UserID] [nvarchar](128) NOT NULL,
	[ApplicationDate] [date] NOT NULL,
	[ManagerNotes] [varchar](2000) NULL,
	[IsDeclined] [bit] NOT NULL,
	[ResumeFilename] [varchar](75) NULL,
 CONSTRAINT [PK_Applications] PRIMARY KEY CLUSTERED 
(
	[ApplicationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 10/23/2018 2:23:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetRoles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 10/23/2018 2:23:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUserClaims]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 10/23/2018 2:23:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUserLogins]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 10/23/2018 2:23:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 10/23/2018 2:23:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUsers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Locations]    Script Date: 10/23/2018 2:23:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Locations]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Locations](
	[LocationID] [int] IDENTITY(1,1) NOT NULL,
	[CompanyName] [varchar](50) NOT NULL,
	[City] [varchar](50) NOT NULL,
	[State] [char](2) NOT NULL,
	[CompanyID] [nvarchar](128) NOT NULL,
	[CompanyPic] [varchar](128) NULL,
 CONSTRAINT [PK_Locations] PRIMARY KEY CLUSTERED 
(
	[LocationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OpenPositions]    Script Date: 10/23/2018 2:23:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[OpenPositions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[OpenPositions](
	[OpenPositionID] [int] IDENTITY(1,1) NOT NULL,
	[PositionID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
 CONSTRAINT [PK_OpenPositions] PRIMARY KEY CLUSTERED 
(
	[OpenPositionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Positions]    Script Date: 10/23/2018 2:23:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Positions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Positions](
	[PositionID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[JobDescription] [varchar](max) NULL,
 CONSTRAINT [PK_Positions] PRIMARY KEY CLUSTERED 
(
	[PositionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserDetails]    Script Date: 10/23/2018 2:23:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserDetails]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserDetails](
	[UserID] [nvarchar](128) NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[ResumeFilename] [varchar](150) NOT NULL,
 CONSTRAINT [PK_UserDetails] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[__MigrationHistory] ([MigrationId], [ContextKey], [Model], [ProductVersion]) VALUES (N'201810151837244_InitialCreate', N'IdentitySample.Models.ApplicationDbContext', 0x1F8B0800000000000400DD5CDB6EE436127D5F60FF41D0D3EEC269F99219CC1AED044EDBDE3532BE60DA93DDB7015B62B785912845A21C1B41BE6C1FF693F20B294AD485375DBAE5EE7610209816C953C5E221592C16FDFBFFFE3FFDFE390CAC279CA47E44CEECA3C9A16D61E2469E4F5667764697DF7CB0BFFFEEAF7F995E7AE1B3F55359EF84D58396243DB31F298D4F1D27751F7188D249E8BB4994464B3A71A3D0415EE41C1F1EFED3393A723040D8806559D34F19A17E88F31FF073161117C73443C14DE4E120E5DFA1649EA35AB728C4698C5C7C665F7B18DAD297390AE3004F8A06B6751EF8089499E360695B889088220AAA9E7E4EF19C261159CD63F8808287971843BD250A52CCBB705A57EFDB9BC363D61BA76E5842B9594AA37020E0D109378F23375FCBC876653E30E0656E2CD6EBDC88B5FD3E4501184016783A0B1256F9CCBEA9449CA7F12DA693B2E1A480BC4A00EE9728F93A69221E58BDDB1D54743A9E1CB2FF0EAC5916D02CC16704673441C181759F2D02DFFD11BF3C445F31393B395A2C4F3EBC7B8FBC93F7DFE29377CD9E425FA19EF0013EDD27518C13D00D2FABFEDB9623B673E48655B3469BC22AC0259819B675839E3F62B2A28F30678E3FD8D695FF8CBDF20B27D767E2C344824634C9E0E76D16046811E0AADC6995C9FEDF22F5F8DDFB51A4DEA2277F950FBD241F264E02F3EA130EF2D2F4D18F8BE9258CF7175EED2A8942F65BE45751FA651E6589CB3A1319AB3CA06485A9A8DDD4A9C9DB8BD20C6A7C5A97A8FB4F6DA6A94A6F6D55D6A17566422962DBB3A1D4F775E5F666DC791CC3E0E5D4621669239C76BF9A480007D67FF0A266CD515FD610E8CD9F7911BC0C911F8CB00AF690023EC8D24F425CF5F287083887C8609DEF519AC222E0FD1BA58F2DAAC33F47507D8EDD2C61ECA240AF579776FF18117C9B850B46F9EDC91A6D681E7E89AE904BA3E492B0561BE37D8CDCAF51462F89778128FE4CDD1290FD7CF0C3FE00A3A873EEBA384DAF80CCD89B45E0629780D7849E1C0F86630BD3AEBD905980FC50EF86484BE897B26AED8AE86B28EE88A19ACE256953F563B4F2493F55CBAA66558B1A9DAAF26A43556560FD34E535CD8AE6153AF52C6A8DE6E4E52334BE9797C3EEBF9BB7D9E66D5A0B1A669CC30A89FF85094E6019F3EE11A53821F508F4593776E12CE4C3C784BEFADE944BFA0905D9D8A2D69A0DF92230FE6CC861F77F36E46AC2E727DF635E498FB34F5919E07BD5D71FABBAE79CA4D9B6A783D0CD6D0BDFCE1A609A2EE7691AB97E3E0B34512F1EB310F5071FCEEA0E6014BD918320D03120BACFB63CF8027DB36552DD910B1C608AAD73B7880ACE50EA224F352374C81BA058B9A36A14AB8321A272FF506402D371C21A2176084A61A6FA84AAD3C227AE1FA3A0D34A52CB9E5B18EB7B25432EB9C031264C60A725FA08D7C73E9802951C6950BA2C34751A8C6B27A2C16B358D79970B5B8FBB1292D80A273B7C67032FB9FFF62AC46CB7D816C8D96E923E0A18E378BB20283FABF425807C70D937824A27260341B94BB515828A16DB01414593BC39821647D4BEE32F9D57F78D9EE24179FBDB7AABB976C04DC11E7B46CDC2F78436145AE044A5E7C58215E267AA399C819EFC7C96725757A6487E6980A918B2A9FD5DAD1FEAB483C8246A03AC89D601CA6F00152065420D50AE8CE5B56AC7BD8801B065DCAD1596AFFD126C83032A76F326B451D17C5F2A93B3D7E9A3EA59C50685E4BD0E0B0D1C0D21E4C54BEC780FA398E2B2AA61FAF8C243BCE146C7F860B418A8C3733518A9ECCCE8562AA9D96D259D4336C425DBC84A92FB64B052D999D1ADC439DA6D248D5330C02DD8C844E2163ED2642B231DD56E53954D9D22478A7F983A8664AAE90D8A639FAC1AC955FC8B352F32AB66DFCC87E71B850586E3A69AB4A34ADB4A128D12B4C2522988064DAFFC24A51788A20562719E99172AD5B47BAB61F92F4536B74F7510CB7DA0ACCDFE2DAEEDE2BDBDB0DDAAFE0887B9824E86CCA9C923E91A0AE89B5B2CDF0D0528D104EF67519085C4EC63995B175778CDF6C5171561EA48FA2B3E946230C5D315ADDF6B6CD47931DE38555ECCFA6365863059BCF4419B3637F9A56694324CD5443185AE763676267766E878C9CEE2F0E1EA44789DD9C533549A00FCD3408C46928302D628EB8F2AE6A13431C592FE8852B24913522A1AA06533A54450B259B0169EC1A2FA1AFD25A849244D74B5B43FB2269DA409AD295E035BA3B35CD61F559371D204D614F7C7AED34FE475748FF72FE31166930DAC38E86EB68319305E67511C67036CDCE737811A9F0762F11B7B058C7FDF4B42194F7B9B10AA08716C4628038679FD112EC3C5E5A7F506DF8C29DC700B4B7CDB0DBF196F186D5F951CCA794FAE5249AFCE7DD2F96ECACF5ADD2F6A94C35751C5B64A33C2F6FE92521C4E5885C9FCE76016F8982DE665851B44FC254E6991D5611F1F1E1D4B2F72F6E7758C93A65EA039AB9A9EC88863B685042DF28412F711256ABAC4062F486A5025127D4D3CFC7C66FF9AB73ACD831AEC5FF9E703EB3AFD4CFC9F33287848326CFDA6A67F8E9351DF7EDADAD3F70FFDAD7AFDDF2F45D303EB2E8119736A1D4AB65C6784C5571183B4299A6EA0CDDA6F25DEEE84125E236851A509B1FEE383854F477978506AF9B7103DFF7DA86ADAC7051B216A1E108C85378A094D0F04D6C1323E0EF0E027CD1F070CEBACFEB1C03AAA191F0AF8643898FC4CA0FF3254B6DCE156A339166D6349CAEDDC9966BD51CEE5AEF726251B7BA389AE665C0F80DB20AB7A0D66BCB184E4D176474DBEF168D8BBA4F6AB2719EF4B5E719DF1B1DB74E26D6610B7DC0FFDA91287F720D54D93BAB3FBF4E06D73CD14CADDF31CCB6149C07B46369ED0B5FB54DF6D93CD14E6DD73B20D4AE8DD33AEED6AFFDC31D37A6FA13B4FCF55338D0C5732BA587057FA6D11388713FE220212141E65F16A529FEF65125693C528B0AE62166A4E3493052B134791ABD468173BACAF7CC36FED2CAFD32ED6909ED9269BAFFFADB2799D76D986A4C75D240E6BD30E75C9DC1DEB585B36D45B4A14167AD29197DEE5B3B6DEAFBFA5BCE0518C22CC1EC31DF1DB49031EC524634E9D0169BFEA752FEC9D8D3FB308FB77EAAF6A08F647170976855DB3AA734D9651B9794B1A9555A408CD0DA6C8832DF53CA1FE12B9148A598C397FF69DC7EDD84DC7027BD7E42EA37146A1CB385C0442C08B39016DF2F3DC6651E7E95D9CFF059331BA006AFA2C367F477EC8FCC0ABF4BED2C4840C10CCBBE0115D3696944576572F15D26D447A0271F3554ED1030EE300C0D23B32474F781DDD807E1FF10AB92F7504D004D23D10A2D9A7173E5A25284C3946DD1E7E0287BDF0F9BB3F007FEB70246D540000, N'6.1.3-40302')
SET IDENTITY_INSERT [dbo].[Applications] ON 

INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (6, 15, N'1bb2124f-fb88-4fd8-bee3-08980b3987aa', CAST(0xDC3E0B00 AS Date), NULL, 0, N'Countries.txt')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (7, 17, N'1bb2124f-fb88-4fd8-bee3-08980b3987aa', CAST(0xDC3E0B00 AS Date), NULL, 0, N'Countries.txt')
INSERT [dbo].[Applications] ([ApplicationID], [OpenPositionID], [UserID], [ApplicationDate], [ManagerNotes], [IsDeclined], [ResumeFilename]) VALUES (8, 15, N'23c2b595-397c-4eb0-9f98-535ff4ee1614', CAST(0xDD3E0B00 AS Date), NULL, 1, N'Countries.txt')
SET IDENTITY_INSERT [dbo].[Applications] OFF
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'd41aab07-e1b8-49f6-a3eb-95a6a6c2b3ae', N'Admin')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'52bf9160-14b3-4442-85c8-279d987a62a4', N'Company')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'2714c247-6aad-487c-977b-e7bb648db376', N'User')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'1bb2124f-fb88-4fd8-bee3-08980b3987aa', N'2714c247-6aad-487c-977b-e7bb648db376')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'23c2b595-397c-4eb0-9f98-535ff4ee1614', N'2714c247-6aad-487c-977b-e7bb648db376')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'46366583-d1d6-4e21-9207-969e3e1590a1', N'52bf9160-14b3-4442-85c8-279d987a62a4')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'4fb4b375-ef5b-44de-81c5-f0c349ff22f2', N'52bf9160-14b3-4442-85c8-279d987a62a4')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'bdb8e006-c199-469f-b3e0-6bf19e5187af', N'52bf9160-14b3-4442-85c8-279d987a62a4')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'f5af10df-fe23-4ed7-9ab1-2b508eb9ca9d', N'52bf9160-14b3-4442-85c8-279d987a62a4')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'40399813-c1dd-45c0-9839-022140bce847', N'd41aab07-e1b8-49f6-a3eb-95a6a6c2b3ae')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'1bb2124f-fb88-4fd8-bee3-08980b3987aa', N'UserTwo@gmail.com', 0, N'ANnL8TtdVD3CuAOww4kD2YAX8R/XHmFEQLoYswogO6dXJ9WxcOo/xblTXLG6HTl0xw==', N'54111d5c-b6cb-4e66-8ded-a9d23a3e46ab', NULL, 0, 0, NULL, 1, 0, N'UserTwo@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'1d2ad115-9f61-44ad-b5fc-20b2138ddba8', N'UserSeven@gmail.com', 0, N'AGrmNC+griavQGBFedOibNhQ0d0Hj6ynDjLIxoj3x6Ic3jFWcDCsBgd2gtesGd6VyA==', N'9e8672e0-41ba-4a80-8154-cd0d3060ebda', NULL, 0, 0, NULL, 1, 0, N'UserSeven@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'23c2b595-397c-4eb0-9f98-535ff4ee1614', N'u1@gmail.com', 0, N'AOdCYMH0G8fcCXnsqwfkC+mgTCneZpmKbW4AtvYC82P+7xPkEavFOQ9GCFaLhZcLxg==', N'3f6b6d37-06af-45ea-a6e8-a1c2767e5458', NULL, 0, 0, NULL, 1, 0, N'u1@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'40399813-c1dd-45c0-9839-022140bce847', N'admin@example.com', 0, N'APiRec1fZU6ccqBMLfbZJ5Y70IK09z2dSFFUw3L3x8VjoA/tffBVK9P8Zy4X9FkNew==', N'fca7eae7-19cf-468a-9a81-c7079994450d', NULL, 0, 0, NULL, 0, 0, N'admin@example.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'46366583-d1d6-4e21-9207-969e3e1590a1', N'companyOne@gmail.com', 0, N'AJzwWaIkl+QwIkEnkFBqGo0k3g6Cv69foIhx1VHT0m3VYgFjO5ZplhYXUj1MmYup7g==', N'68ffe6ce-c475-4eeb-9c03-52a1cb071f93', NULL, 0, 0, NULL, 1, 0, N'companyOne@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'4fb4b375-ef5b-44de-81c5-f0c349ff22f2', N'companyThree@gmail.com', 0, N'AKH3I9Rvd4XQjmRQEFO+YTyMdXYKS61YTCLMyI2uyMYeRFCVqSp+az9xz/ryWHpzmg==', N'60c22631-5ae0-4179-a487-4fba719b0be6', NULL, 0, 0, NULL, 1, 0, N'companyThree@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'bdb8e006-c199-469f-b3e0-6bf19e5187af', N'sablevonhoover@gmail.com', 0, N'AGxzi1S2xbsyWPFiRhIM5fMZqdb3xaPOSPUE6asEZi5bGiHT1UT9rncli2PDLPKt5A==', N'c8e6a234-9f74-42a5-93dd-617599d735e7', NULL, 0, 0, NULL, 1, 0, N'sablevonhoover@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'f5af10df-fe23-4ed7-9ab1-2b508eb9ca9d', N'c1@gmail.com', 0, N'AMPIGTOH3IyOmQ7H/oDL1ZRYqIBhk7+mxzSt02BaPCVfdYXAskoYsQQJmmNrDnmzXA==', N'9d467e4d-3d32-4b00-8610-4874e00765a4', NULL, 0, 0, NULL, 1, 0, N'c1@gmail.com')
SET IDENTITY_INSERT [dbo].[Locations] ON 

INSERT [dbo].[Locations] ([LocationID], [CompanyName], [City], [State], [CompanyID], [CompanyPic]) VALUES (11, N'Salty Pirahana', N'Bozeman', N'MT', N'46366583-d1d6-4e21-9207-969e3e1590a1', N'nopic.png')
INSERT [dbo].[Locations] ([LocationID], [CompanyName], [City], [State], [CompanyID], [CompanyPic]) VALUES (12, N'Zach''s Snax', N'Olathe', N'KS', N'ca25600c-274b-4acd-a4ba-15b3a229e9b7', N'e7d47cc1-a3f1-4bc5-9394-ac0cff67f204.jpg')
INSERT [dbo].[Locations] ([LocationID], [CompanyName], [City], [State], [CompanyID], [CompanyPic]) VALUES (13, N'BookBag Express 7 Studio L', N'Dallas', N'TX', N'a37b9873-c4ab-4ea1-9186-87728d691fc6', N'0942fb7a-5d94-4b2b-a7f4-41c61aa85624.png')
INSERT [dbo].[Locations] ([LocationID], [CompanyName], [City], [State], [CompanyID], [CompanyPic]) VALUES (14, N'Spicy Games', N'Reno', N'NV', N'4fb4b375-ef5b-44de-81c5-f0c349ff22f2', N'b718d733-551c-4fba-ae2e-bc80a629b823.jpg')
INSERT [dbo].[Locations] ([LocationID], [CompanyName], [City], [State], [CompanyID], [CompanyPic]) VALUES (15, N'Test', N'Test', N'CA', N'f5af10df-fe23-4ed7-9ab1-2b508eb9ca9d', N'5d283a56-98b8-45c8-b12e-19a743d44803.jpg')
SET IDENTITY_INSERT [dbo].[Locations] OFF
SET IDENTITY_INSERT [dbo].[OpenPositions] ON 

INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (14, 1, 11)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (15, 2, 12)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (16, 3, 12)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (17, 1, 12)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (18, 3, 12)
INSERT [dbo].[OpenPositions] ([OpenPositionID], [PositionID], [LocationID]) VALUES (19, 2, 14)
SET IDENTITY_INSERT [dbo].[OpenPositions] OFF
SET IDENTITY_INSERT [dbo].[Positions] ON 

INSERT [dbo].[Positions] ([PositionID], [Title], [JobDescription]) VALUES (1, N'Game Developer -- Unity', N'The Game Developer role is responsible for building games for various target platforms based on the Unity framework.')
INSERT [dbo].[Positions] ([PositionID], [Title], [JobDescription]) VALUES (2, N'Game Test Analyst', N'Game Testers at PlayStation are responsible for finding and replicating bugs/glitches and accurately reporting them to the game developers.')
INSERT [dbo].[Positions] ([PositionID], [Title], [JobDescription]) VALUES (3, N'Intern', N'Makes Coffee')
INSERT [dbo].[Positions] ([PositionID], [Title], [JobDescription]) VALUES (4, N'Bob', N'Yo')
SET IDENTITY_INSERT [dbo].[Positions] OFF
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFilename]) VALUES (N'1bb2124f-fb88-4fd8-bee3-08980b3987aa', N'Test', N'Guy', N'Countries.txt')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFilename]) VALUES (N'23c2b595-397c-4eb0-9f98-535ff4ee1614', N'Lief', N'Erikson', N'Countries.txt')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFilename]) VALUES (N'bdb8e006-c199-469f-b3e0-6bf19e5187af', N'Sable', N'Hoover', N'srnsnyst')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName], [ResumeFilename]) VALUES (N'ce59c9c1-e052-41bc-89fd-fce3f8d6c673', N'Bob', N'Boberson', N'Sable Hoover Resume.docx')
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Applications_OpenPositions]') AND parent_object_id = OBJECT_ID(N'[dbo].[Applications]'))
ALTER TABLE [dbo].[Applications]  WITH CHECK ADD  CONSTRAINT [FK_Applications_OpenPositions] FOREIGN KEY([OpenPositionID])
REFERENCES [dbo].[OpenPositions] ([OpenPositionID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Applications_OpenPositions]') AND parent_object_id = OBJECT_ID(N'[dbo].[Applications]'))
ALTER TABLE [dbo].[Applications] CHECK CONSTRAINT [FK_Applications_OpenPositions]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserClaims]'))
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserClaims]'))
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserLogins]'))
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserLogins]'))
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]'))
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]'))
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]'))
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]'))
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenPositions_Locations]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenPositions]'))
ALTER TABLE [dbo].[OpenPositions]  WITH CHECK ADD  CONSTRAINT [FK_OpenPositions_Locations] FOREIGN KEY([LocationID])
REFERENCES [dbo].[Locations] ([LocationID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenPositions_Locations]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenPositions]'))
ALTER TABLE [dbo].[OpenPositions] CHECK CONSTRAINT [FK_OpenPositions_Locations]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenPositions_Positions]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenPositions]'))
ALTER TABLE [dbo].[OpenPositions]  WITH CHECK ADD  CONSTRAINT [FK_OpenPositions_Positions] FOREIGN KEY([PositionID])
REFERENCES [dbo].[Positions] ([PositionID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_OpenPositions_Positions]') AND parent_object_id = OBJECT_ID(N'[dbo].[OpenPositions]'))
ALTER TABLE [dbo].[OpenPositions] CHECK CONSTRAINT [FK_OpenPositions_Positions]
GO
