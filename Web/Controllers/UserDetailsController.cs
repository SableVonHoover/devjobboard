﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Data;

namespace Web.Controllers
{
    public class UserDetailsController : Controller
    {
        private DevJobBoardEntities db = new DevJobBoardEntities();

        // GET: UserDetails
        public ActionResult Index()
        {
            return View(db.UserDetails.ToList());
        }

        // GET: UserDetails/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserDetail userDetail = db.UserDetails.Find(id);
            if (userDetail == null)
            {
                return HttpNotFound();
            }
            return View(userDetail);
        }

        // GET: UserDetails/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UserDetails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UserID,FirstName,LastName,ResumeFilename")] UserDetail userDetail, HttpPostedFileBase resumeUpload)
        {
            if (ModelState.IsValid)
            {
                string resumeName = "nopic.png";
                if (resumeUpload != null)
                {
                    resumeName = resumeUpload.FileName;
                    string ext = resumeName.Substring(resumeName.LastIndexOf("."));
                    string[] goodExts = { ".doc", ".docx", ".txt", ".pdf" };
                    if (goodExts.Contains(ext.ToLower()))
                    {
                        resumeUpload.SaveAs(Server.MapPath("~/Resumes/" + resumeName));
                    }
                    else
                    {
                        resumeName = "noresume.txt";
                    }
                }

                userDetail.ResumeFilename = resumeName;


                db.UserDetails.Add(userDetail);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(userDetail);
        }

        // GET: UserDetails/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserDetail userDetail = db.UserDetails.Find(id);
            if (userDetail == null)
            {
                return HttpNotFound();
            }
            return View(userDetail);
        }

        // POST: UserDetails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UserID,FirstName,LastName,ResumeFilename")] UserDetail userDetail, HttpPostedFileBase resumeUpload)
        {
            if (ModelState.IsValid)
            {
                string resumeName = "nopic.png";
                if (resumeUpload != null)
                {
                    resumeName = resumeUpload.FileName;
                    string ext = resumeName.Substring(resumeName.LastIndexOf("."));
                    string[] goodExts = { ".doc", ".docx", ".txt", ".pdf" };
                    if (goodExts.Contains(ext.ToLower()))
                    {
                        resumeUpload.SaveAs(Server.MapPath("~/Resumes/" + resumeName));
                        userDetail.ResumeFilename = resumeName;
                    }

                }
                db.Entry(userDetail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(userDetail);
        }

        // GET: UserDetails/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserDetail userDetail = db.UserDetails.Find(id);
            if (userDetail == null)
            {
                return HttpNotFound();
            }
            return View(userDetail);
        }

        // POST: UserDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            UserDetail userDetail = db.UserDetails.Find(id);
            db.UserDetails.Remove(userDetail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
