﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Data;
using Data.Services;
using System.Drawing;

namespace Web.Controllers
{
    public class LocationsController : Controller
    {
        private DevJobBoardEntities db = new DevJobBoardEntities();

        // GET: Locations
        public ActionResult Index()
        {
            return View(db.Locations.ToList());
        }

        // GET: Locations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location location = db.Locations.Find(id);
            if (location == null)
            {
                return HttpNotFound();
            }
            return View(location);
        }

        // GET: Locations/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Locations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "LocationID,CompanyName,City,State,CompanyID,CompanyPic")] Location location, HttpPostedFileBase imageUpload)
        {
            if (ModelState.IsValid)
            {
                string fileName = "nopic.png";

                if (imageUpload != null)
                {
                    fileName = imageUpload.FileName;
                    string ext = fileName.Substring(fileName.LastIndexOf("."));
                    string[] goodExts = { ".jpg", ".jpeg", ".png", ".gif" };
                    if (goodExts.Contains(ext.ToLower()))
                    {
                        fileName = Guid.NewGuid() + ext;
                        string savePath = Server.MapPath("~/Content/img/Companies/");
                        Image convertedImage = Image.FromStream(imageUpload.InputStream);
                        int maxSize = 500;
                        //call ResizeImage() from the class
                        ImageServices.ResizeImage(savePath, fileName, convertedImage, maxSize);
                    }
                }

                location.CompanyPic = fileName;
                db.Locations.Add(location);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(location);
        }

        // GET: Locations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location location = db.Locations.Find(id);
            if (location == null)
            {
                return HttpNotFound();
            }
            return View(location);
        }

        // POST: Locations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "LocationID,CompanyName,City,State,CompanyID,CompanyPic")] Location location, HttpPostedFileBase companyPic)
        {
            if (ModelState.IsValid)
            {
                string imageName = "nopic.png";
                if (companyPic != null)
                {
                    imageName = companyPic.FileName;
                    string ext = imageName.Substring(imageName.LastIndexOf("."));
                    string[] goodExts = { ".jpg", ".jpeg", ".png", ".gif" };
                    if (goodExts.Contains(ext.ToLower()))
                    {
                        companyPic.SaveAs(Server.MapPath("~/Content/img/Companies/" + imageName));
                        location.CompanyPic = imageName;
                    }
                }

                db.Entry(location).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(location);
        }

        // GET: Locations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location location = db.Locations.Find(id);
            if (location == null)
            {
                return HttpNotFound();
            }
            return View(location);
        }

        // POST: Locations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Location location = db.Locations.Find(id);
            db.Locations.Remove(location);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
