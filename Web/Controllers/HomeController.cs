﻿using Web.Models;
using System.Web.Mvc;
using System.Net;
using System.Net.Mail;

namespace IdentitySample.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        [HttpGet]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Contact(ContactViewModel contact)
        {
            if (!ModelState.IsValid)
            {
                return View(contact);
            }

            string body = string.Format(
                $"Name: {contact.Name}<br />Email: {contact.Email}<br />Subject: {contact.Subject}<br />Message: {contact.Message}"
            );

            MailMessage msg = new MailMessage(
                "No-reply@sablehoover.com", //from address
                "sablevonhoover@gmail.com", // to address
                contact.Subject, // email subject
                body); // the body of the email message

            msg.IsBodyHtml = true;
            msg.Priority = MailPriority.High;


            SmtpClient client = new SmtpClient("mail.sablehoover.com");
            client.Credentials = new NetworkCredential("no-reply@sablehoover.com", "dreen126_");

            using (client)
            {
                try
                {
                    client.Send(msg);
                }
                catch
                {
                    ViewBag.ErrorMessage = "There was an error sending your email. Please try again.";
                    return View(contact);
                }
            }

            return View("ContactConfirmation", contact);

        }// Contact() POST
    }
}
