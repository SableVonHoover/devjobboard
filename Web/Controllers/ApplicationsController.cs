﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Data;
using Microsoft.AspNet.Identity;

namespace Web.Controllers
{
    public class ApplicationsController : Controller
    {
        private DevJobBoardEntities db = new DevJobBoardEntities();

        // GET: Applications
        public ActionResult Index()
        {
            #region Foo

            List<Application> app = new List<Application>();
            List<Location> loc = new List<Location>();
            List<OpenPosition> op = new List<OpenPosition>();

            if (User.IsInRole("User"))
            {
                foreach (var item in db.Applications)
                {
                    if (item.UserID == User.Identity.GetUserId())
                    {
                        app.Add(item);
                    }
                }


                return View(app);
            }

            if (User.IsInRole("Company"))
            {
                foreach (var item in db.Locations)
                {
                    //matches current (company)user's ID to CompanyIDs in the Location table
                    if (item.CompanyID == User.Identity.GetUserId())
                    {
                        // Add items that do have right ID to the collection
                        loc.Add(item);
                    }
                }
                foreach (var item in db.OpenPositions)
                {
                    //for each ID from above
                    foreach (var iteml in loc)
                    {
                        // Matchs up CompanyIDs from l collection to what's currently in OpenPositions
                        if (item.LocationID == iteml.LocationID)
                        {
                            // Add items from OpenPositions that have same ID as (company)user
                            op.Add(item);
                        }
                    }
                }
                foreach (var item in db.Applications)
                {
                    //for each ID from above
                    foreach (var itemo in op)
                    {
                        // Matchs up CompanyIDs from l collection to what's currently in OpenPositions
                        if (item.OpenPositionID == itemo.OpenPositionID)
                        {
                            // Add items from OpenPositions that have same ID as (company)user
                            app.Add(item);
                        }
                    }
                }
                // Return view with only matching ids
                return View(app);
            }
            #endregion



            var applications = db.Applications.Include(a => a.OpenPosition);
            return View(applications.ToList());
        }

        // GET: Applications/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = db.Applications.Find(id);
            if (application == null)
            {
                return HttpNotFound();
            }
            return View(application);
        }

        // GET: Applications/Create
        public ActionResult Create()
        {
            ViewBag.OpenPositionID = new SelectList(db.OpenPositions, "OpenPositionID", "OpenPositionID");
            return View();
        }

        // POST: Applications/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ApplicationID,OpenPositionID,UserID,ApplicationDate,ManagerNotes,IsDeclined,ResumeFilename")] Application application)
        {
            if (ModelState.IsValid)
            {
                db.Applications.Add(application);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.OpenPositionID = new SelectList(db.OpenPositions, "OpenPositionID", "OpenPositionID", application.OpenPositionID);
            return View(application);
        }

        // GET: Applications/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = db.Applications.Find(id);
            if (application == null)
            {
                return HttpNotFound();
            }
            ViewBag.OpenPositionID = new SelectList(db.OpenPositions, "OpenPositionID", "OpenPositionID", application.OpenPositionID);
            return View(application);
        }

        // POST: Applications/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ApplicationID,OpenPositionID,UserID,ApplicationDate,ManagerNotes,IsDeclined,ResumeFilename")] Application application)
        {
            if (ModelState.IsValid)
            {
                db.Entry(application).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.OpenPositionID = new SelectList(db.OpenPositions, "OpenPositionID", "OpenPositionID", application.OpenPositionID);
            return View(application);
        }

        // GET: Applications/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = db.Applications.Find(id);
            if (application == null)
            {
                return HttpNotFound();
            }
            return View(application);
        }

        // POST: Applications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Application application = db.Applications.Find(id);
            db.Applications.Remove(application);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Apply(int OpenPositionID)
        {
            Application app = new Application();
            UserDetail user = new UserDetail();
            string ppl = User.Identity.GetUserId();
            user = db.UserDetails.Where(x => x.UserID == ppl).Single();

            app.OpenPositionID = OpenPositionID;
            app.UserID = User.Identity.GetUserId();
            app.ResumeFilename = user.ResumeFilename;
            app.ApplicationDate = DateTime.Now;

            db.Applications.Add(app);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

    }
}
