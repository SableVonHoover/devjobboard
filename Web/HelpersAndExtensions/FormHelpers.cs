﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Web.HelpersAndExtensions
{
    public static class FormHelpers
    {
        public static MvcHtmlString StateDropDownListFor<TModel, TValue>
            (this HtmlHelper<TModel> html,//class being extended
            Expression<Func<TModel, TValue>> expression)//LINQ expression param
        {
            //we will use a Dictionary for our collection of StateCode, StateName pairs
            //C# dictionaries are collections with pk/value pairs
            Dictionary<string, string> stateList =
                new Dictionary<string, string>()
                {
                    //Initialization syntax: provide values for the collection
                    //when we instantiate
                    #region States
                {"", " ----- NONE -----" },
                {"AL"," Alabama"},
                {"AK"," Alaska"},
                {"AZ"," Arizona"},
                {"AR"," Arkansas"},
                {"CA"," California"},
                {"CO"," Colorado"},
                {"CT"," Connecticut"},
                {"DE"," Delaware"},
                {"DC"," District of Columbia"},
                {"FL"," Florida"},
                {"GA"," Georgia"},
                {"HI"," Hawaii"},
                {"ID"," Idaho"},
                {"IL"," Illinois"},
                {"IN"," Indiana"},
                {"IA"," Iowa"},
                {"KS"," Kansas"},
                {"KY"," Kentucky"},
                {"LA"," Louisiana"},
                {"ME"," Maine"},
                {"MD"," Maryland"},
                {"MA"," Massachusetts"},
                {"MI"," Michigan"},
                {"MN"," Minnesota"},
                {"MS"," Mississippi"},
                {"MO"," Missouri"},
                {"MT"," Montana"},
                {"NE"," Nebraska"},
                {"NV"," Nevada"},
                {"NH"," New Hampshire"},
                {"NJ"," New Jersey"},
                {"NM"," New Mexico"},
                {"NY"," New York"},
                {"NC"," North Carolina"},
                {"ND"," North Dakota"},
                {"OH"," Ohio"},
                {"OK"," Oklahoma"},
                {"OR"," Oregon"},
                {"PA"," Pennsylvania"},
                {"RI"," Rhode Island"},
                {"SC"," South Carolina"},
                {"SD"," South Dakota"},
                {"TN"," Tennessee"},
                {"TX"," Texas"},
                {"UT"," Utah"},
                {"VT"," Vermont"},
                {"VA"," Virginia"},
                {"WA"," Washington"},
                {"WV"," West Virginia"},
                {"WI"," Wisconsin"},
                {"WY"," Wyoming"}
                    #endregion
                };

            return html.DropDownListFor(
                expression,//allows us to pass lambda expressions
                new SelectList(stateList, "key", "value"),//datasource, value, displayText
                new { @class = "form-control" });//bootstrap class


        }



    }
}