﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class ContactViewModel
    {
        // We added our attributes for validation. This could also contain our metadata/other data annotations.
        [Required(ErrorMessage = "* Required")]
        public string Name { get; set; }

        [EmailAddress(ErrorMessage = "* Please enter a valid Email")]
        //[RegularExpression(@"Reg Ex Goes Here")]
        [Required(ErrorMessage = "* Required")]
        public string Email { get; set; }

        [Required(ErrorMessage = "* Required")]
        [StringLength(25, ErrorMessage = "* Subjects cannot exceed 25 characters")]
        public string Subject { get; set; }

        [Required(ErrorMessage = "* Required")]
        public string Message { get; set; }

    }
}